resource "aws_instance" "this" {

  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  instance_type               = var.instance_type
  key_name                    = var.key_name
  availability_zone           = var.availability_zone
  vpc_security_group_ids      = [aws_security_group.this.id]
  subnet_id                   = var.subnet_id

  root_block_device {
    volume_size = "40"
    volume_type = "standard"
  }

  ebs_block_device {
    device_name = "/dev/xvdba"
    volume_size = var.ebs_size
    volume_type = "gp3"
    tags = {
      Name = var.name
    }
  }

  tags = {
    Name = var.name
  }
}

# We could independently create the volume and attach it later with "aws_volume_attachment" resource.
# In the original main.tf, EBS volumes are in the same AZ, but instances are in different ones. 
# AWS needs you to attach the EBS volume to an instance in the same AZ which means you cannot attach a volume in 1a to an instance in 1b.

/* resource "aws_ebs_volume" "this" {
  availability_zone = var.ebs_availability_zone
  size              = var.ebs_size
  tags = {
    Name = var.name
  }
}
*/


# SG and ingress rules. To reconcil the state, pre-existing SG should be imported like this:
# terraform import module.ipfs1.this sg-id

resource "aws_security_group" "this" {
  name        = var.name
  description = "Allowed ingress ports"
  vpc_id      = var.vpc_id

  tags = {
    Name = var.name
  }
}

resource "aws_security_group_rule" "ingress_4001" {
  type              = "ingress"
  from_port         = 4001
  to_port           = 4001
  protocol          = "all"
  security_group_id = aws_security_group.this.id
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Port 4001"
}

resource "aws_security_group_rule" "ingress_8080" {
  type              = "ingress"
  from_port         = 8080
  to_port           = 8080
  protocol          = "all"
  security_group_id = aws_security_group.this.id
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Port 8080"
}

resource "aws_security_group_rule" "ingress_22" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "all"
  security_group_id = aws_security_group.this.id
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Port 22"
}
