# Some outputs examples
output "ami" {
  description = "AMI for the EC2 instance"
  value       = aws_instance.this.ami
}

output "subnet_id" {
  description = "Subnet ID where the EC2 instance is"
  value       = aws_instance.this.subnet_id
}

# etc, etc
