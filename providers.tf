terraform {
  required_providers {
    aws = {
      version = "4.60.0" # Last version
    }
  }
}

provider "aws" {
  region = var.region
  #profile = var.aws_profile
  #assume_role {
  #  role_arn = var.role_arn
  #}
}