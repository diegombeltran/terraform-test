# Main tf file
# Common attributes rewritten as variables. It's a simple stack so maybe it's simpler to hardcode the "exclusive" attributes (AZ and subnet IDs).

module "ipfs1" {
  source = "./modules/ec2-instance"

  name                        = "ipfs1"
  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  instance_type               = var.instance_type
  key_name                    = var.key_name
  availability_zone           = "eu-west-1a"
  subnet_id                   = "private-subnetid-1"
  ebs_size                    = var.ebs_size
  vpc_id                      = var.vpc_id
}

module "ipfs2" {
  source = "./modules/ec2-instance"

  name                        = "ipfs2"
  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  instance_type               = var.instance_type
  key_name                    = var.key_name
  availability_zone           = "eu-west-1b"
  subnet_id                   = "private-subnetid-2"
  ebs_size                    = var.ebs_size
  vpc_id                      = var.vpc_id
}

module "ipfs3" {
  source = "./modules/ec2-instance"

  name                        = "ipfs3"
  ami                         = var.ami
  associate_public_ip_address = var.associate_public_ip_address
  instance_type               = var.instance_type
  key_name                    = var.key_name
  availability_zone           = "eu-west-1c"
  subnet_id                   = "private-subnetid-3"
  ebs_size                    = var.ebs_size
  vpc_id                      = var.vpc_id
}