
Terraform used the selected providers to generate the following execution
plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.ipfs1.aws_instance.this will be created
  + resource "aws_instance" "this" {
      + ami                                  = "ami-abcd1234"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = false
      + availability_zone                    = "eu-west-1a"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.medium"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "ipfs-keypair-name"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = (known after apply)
      + source_dest_check                    = true
      + subnet_id                            = "private-subnetid-1"
      + tags                                 = {
          + "Name" = "ipfs1"
        }
      + tags_all                             = {
          + "Name" = "ipfs1"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + ebs_block_device {
          + delete_on_termination = true
          + device_name           = "/dev/xvdba"
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = {
              + "Name" = "ipfs1"
            }
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = 100
          + volume_type           = "gp3"
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = 40
          + volume_type           = "standard"
        }
    }

  # module.ipfs1.aws_security_group.this will be created
  + resource "aws_security_group" "this" {
      + arn                    = (known after apply)
      + description            = "Allowed ingress ports"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = "ipfs1"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name" = "ipfs1"
        }
      + tags_all               = {
          + "Name" = "ipfs1"
        }
      + vpc_id                 = "myVPCId"
    }

  # module.ipfs1.aws_security_group_rule.ingress_22 will be created
  + resource "aws_security_group_rule" "ingress_22" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 22"
      + from_port                = 22
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 22
      + type                     = "ingress"
    }

  # module.ipfs1.aws_security_group_rule.ingress_4001 will be created
  + resource "aws_security_group_rule" "ingress_4001" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 4001"
      + from_port                = 4001
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 4001
      + type                     = "ingress"
    }

  # module.ipfs1.aws_security_group_rule.ingress_8080 will be created
  + resource "aws_security_group_rule" "ingress_8080" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 8080"
      + from_port                = 8080
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 8080
      + type                     = "ingress"
    }

  # module.ipfs2.aws_instance.this will be created
  + resource "aws_instance" "this" {
      + ami                                  = "ami-abcd1234"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = false
      + availability_zone                    = "eu-west-1b"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.medium"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "ipfs-keypair-name"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = (known after apply)
      + source_dest_check                    = true
      + subnet_id                            = "private-subnetid-2"
      + tags                                 = {
          + "Name" = "ipfs2"
        }
      + tags_all                             = {
          + "Name" = "ipfs2"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + ebs_block_device {
          + delete_on_termination = true
          + device_name           = "/dev/xvdba"
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = {
              + "Name" = "ipfs2"
            }
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = 100
          + volume_type           = "gp3"
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = 40
          + volume_type           = "standard"
        }
    }

  # module.ipfs2.aws_security_group.this will be created
  + resource "aws_security_group" "this" {
      + arn                    = (known after apply)
      + description            = "Allowed ingress ports"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = "ipfs2"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name" = "ipfs2"
        }
      + tags_all               = {
          + "Name" = "ipfs2"
        }
      + vpc_id                 = "myVPCId"
    }

  # module.ipfs2.aws_security_group_rule.ingress_22 will be created
  + resource "aws_security_group_rule" "ingress_22" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 22"
      + from_port                = 22
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 22
      + type                     = "ingress"
    }

  # module.ipfs2.aws_security_group_rule.ingress_4001 will be created
  + resource "aws_security_group_rule" "ingress_4001" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 4001"
      + from_port                = 4001
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 4001
      + type                     = "ingress"
    }

  # module.ipfs2.aws_security_group_rule.ingress_8080 will be created
  + resource "aws_security_group_rule" "ingress_8080" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 8080"
      + from_port                = 8080
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 8080
      + type                     = "ingress"
    }

  # module.ipfs3.aws_instance.this will be created
  + resource "aws_instance" "this" {
      + ami                                  = "ami-abcd1234"
      + arn                                  = (known after apply)
      + associate_public_ip_address          = false
      + availability_zone                    = "eu-west-1c"
      + cpu_core_count                       = (known after apply)
      + cpu_threads_per_core                 = (known after apply)
      + disable_api_stop                     = (known after apply)
      + disable_api_termination              = (known after apply)
      + ebs_optimized                        = (known after apply)
      + get_password_data                    = false
      + host_id                              = (known after apply)
      + host_resource_group_arn              = (known after apply)
      + iam_instance_profile                 = (known after apply)
      + id                                   = (known after apply)
      + instance_initiated_shutdown_behavior = (known after apply)
      + instance_state                       = (known after apply)
      + instance_type                        = "t2.medium"
      + ipv6_address_count                   = (known after apply)
      + ipv6_addresses                       = (known after apply)
      + key_name                             = "ipfs-keypair-name"
      + monitoring                           = (known after apply)
      + outpost_arn                          = (known after apply)
      + password_data                        = (known after apply)
      + placement_group                      = (known after apply)
      + placement_partition_number           = (known after apply)
      + primary_network_interface_id         = (known after apply)
      + private_dns                          = (known after apply)
      + private_ip                           = (known after apply)
      + public_dns                           = (known after apply)
      + public_ip                            = (known after apply)
      + secondary_private_ips                = (known after apply)
      + security_groups                      = (known after apply)
      + source_dest_check                    = true
      + subnet_id                            = "private-subnetid-3"
      + tags                                 = {
          + "Name" = "ipfs3"
        }
      + tags_all                             = {
          + "Name" = "ipfs3"
        }
      + tenancy                              = (known after apply)
      + user_data                            = (known after apply)
      + user_data_base64                     = (known after apply)
      + user_data_replace_on_change          = false
      + vpc_security_group_ids               = (known after apply)

      + ebs_block_device {
          + delete_on_termination = true
          + device_name           = "/dev/xvdba"
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = {
              + "Name" = "ipfs3"
            }
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = 100
          + volume_type           = "gp3"
        }

      + root_block_device {
          + delete_on_termination = true
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = 40
          + volume_type           = "standard"
        }
    }

  # module.ipfs3.aws_security_group.this will be created
  + resource "aws_security_group" "this" {
      + arn                    = (known after apply)
      + description            = "Allowed ingress ports"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = "ipfs3"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name" = "ipfs3"
        }
      + tags_all               = {
          + "Name" = "ipfs3"
        }
      + vpc_id                 = "myVPCId"
    }

  # module.ipfs3.aws_security_group_rule.ingress_22 will be created
  + resource "aws_security_group_rule" "ingress_22" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 22"
      + from_port                = 22
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 22
      + type                     = "ingress"
    }

  # module.ipfs3.aws_security_group_rule.ingress_4001 will be created
  + resource "aws_security_group_rule" "ingress_4001" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 4001"
      + from_port                = 4001
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 4001
      + type                     = "ingress"
    }

  # module.ipfs3.aws_security_group_rule.ingress_8080 will be created
  + resource "aws_security_group_rule" "ingress_8080" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "Port 8080"
      + from_port                = 8080
      + id                       = (known after apply)
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + security_group_rule_id   = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 8080
      + type                     = "ingress"
    }

Plan: 15 to add, 0 to change, 0 to destroy.

─────────────────────────────────────────────────────────────────────────────

Note: You didn't use the -out option to save this plan, so Terraform can't
guarantee to take exactly these actions if you run "terraform apply" now.
