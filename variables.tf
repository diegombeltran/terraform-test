variable "region" {
  default = "eu-west-1"
}

variable "aws_profile" {
  default = "whatever"
}

variable "role_arn" {
  default = "arn:aws:iam::accountnumber:role/adminrole"
}

variable "vpc_id" {
  default = "myVPCId"
}

# EC2 instances
variable "ami" {}
variable "associate_public_ip_address" {}
variable "instance_type" {}
variable "key_name" {}
variable "ebs_size" {}