/* This is an example in case we need to access data from another state file (To access VPC settings, for example)

data "terraform_remote_state" "shared" {
  backend = "s3"

  config = {
    bucket = var.terraform_tfstate_bucket
    key    = var.shared_tfstate_key
    region = var.region
  }
}
*/

# This is our main tfstate file. We could improve this by using DynamoDB to provide a lock file.
terraform {
  backend "s3" {
  }
}

